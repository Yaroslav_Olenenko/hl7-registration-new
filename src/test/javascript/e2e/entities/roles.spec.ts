import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Roles e2e test', () => {

    let navBarPage: NavBarPage;
    let rolesDialogPage: RolesDialogPage;
    let rolesComponentsPage: RolesComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Roles', () => {
        navBarPage.goToEntity('roles');
        rolesComponentsPage = new RolesComponentsPage();
        expect(rolesComponentsPage.getTitle()).toMatch(/Roles/);

    });

    it('should load create Roles dialog', () => {
        rolesComponentsPage.clickOnCreateButton();
        rolesDialogPage = new RolesDialogPage();
        expect(rolesDialogPage.getModalTitle()).toMatch(/Create or edit a Roles/);
        rolesDialogPage.close();
    });

    it('should create and save Roles', () => {
        rolesComponentsPage.clickOnCreateButton();
        rolesDialogPage.setRoleNameInput('roleName');
        expect(rolesDialogPage.getRoleNameInput()).toMatch('roleName');
        // rolesDialogPage.userToRoleSelectLastOption();
        rolesDialogPage.save();
        expect(rolesDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RolesComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-roles div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class RolesDialogPage {
    modalTitle = element(by.css('h4#myRolesLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    roleNameInput = element(by.css('input#field_roleName'));
    userToRoleSelect = element(by.css('select#field_userToRole'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setRoleNameInput = function (roleName) {
        this.roleNameInput.sendKeys(roleName);
    }

    getRoleNameInput = function () {
        return this.roleNameInput.getAttribute('value');
    }

    userToRoleSelectLastOption = function () {
        this.userToRoleSelect.all(by.tagName('option')).last().click();
    }

    userToRoleSelectOption = function (option) {
        this.userToRoleSelect.sendKeys(option);
    }

    getUserToRoleSelect = function () {
        return this.userToRoleSelect;
    }

    getUserToRoleSelectedOption = function () {
        return this.userToRoleSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}

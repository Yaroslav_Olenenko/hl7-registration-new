import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Calendarrecption e2e test', () => {

    let navBarPage: NavBarPage;
    let calendarrecptionDialogPage: CalendarrecptionDialogPage;
    let calendarrecptionComponentsPage: CalendarrecptionComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Calendarrecptions', () => {
        navBarPage.goToEntity('calendarrecption');
        calendarrecptionComponentsPage = new CalendarrecptionComponentsPage();
        expect(calendarrecptionComponentsPage.getTitle()).toMatch(/Calendarrecptions/);

    });

    it('should load create Calendarrecption dialog', () => {
        calendarrecptionComponentsPage.clickOnCreateButton();
        calendarrecptionDialogPage = new CalendarrecptionDialogPage();
        expect(calendarrecptionDialogPage.getModalTitle()).toMatch(/Create or edit a Calendarrecption/);
        calendarrecptionDialogPage.close();
    });

    it('should create and save Calendarrecptions', () => {
        calendarrecptionComponentsPage.clickOnCreateButton();
        calendarrecptionDialogPage.setDateInput('2000-12-31');
        expect(calendarrecptionDialogPage.getDateInput()).toMatch('2000-12-31');
        calendarrecptionDialogPage.setAdressInput('adress');
        expect(calendarrecptionDialogPage.getAdressInput()).toMatch('adress');
        // calendarrecptionDialogPage.calendarreceptionuserSelectLastOption();
        calendarrecptionDialogPage.save();
        expect(calendarrecptionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class CalendarrecptionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-calendarrecption div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class CalendarrecptionDialogPage {
    modalTitle = element(by.css('h4#myCalendarrecptionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateInput = element(by.css('input#field_date'));
    adressInput = element(by.css('input#field_adress'));
    calendarreceptionuserSelect = element(by.css('select#field_calendarreceptionuser'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setDateInput = function (date) {
        this.dateInput.sendKeys(date);
    }

    getDateInput = function () {
        return this.dateInput.getAttribute('value');
    }

    setAdressInput = function (adress) {
        this.adressInput.sendKeys(adress);
    }

    getAdressInput = function () {
        return this.adressInput.getAttribute('value');
    }

    calendarreceptionuserSelectLastOption = function () {
        this.calendarreceptionuserSelect.all(by.tagName('option')).last().click();
    }

    calendarreceptionuserSelectOption = function (option) {
        this.calendarreceptionuserSelect.sendKeys(option);
    }

    getCalendarreceptionuserSelect = function () {
        return this.calendarreceptionuserSelect;
    }

    getCalendarreceptionuserSelectedOption = function () {
        return this.calendarreceptionuserSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}

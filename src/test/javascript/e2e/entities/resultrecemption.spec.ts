import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Resultrecemption e2e test', () => {

    let navBarPage: NavBarPage;
    let resultrecemptionDialogPage: ResultrecemptionDialogPage;
    let resultrecemptionComponentsPage: ResultrecemptionComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Resultrecemptions', () => {
        navBarPage.goToEntity('resultrecemption');
        resultrecemptionComponentsPage = new ResultrecemptionComponentsPage();
        expect(resultrecemptionComponentsPage.getTitle()).toMatch(/Resultrecemptions/);

    });

    it('should load create Resultrecemption dialog', () => {
        resultrecemptionComponentsPage.clickOnCreateButton();
        resultrecemptionDialogPage = new ResultrecemptionDialogPage();
        expect(resultrecemptionDialogPage.getModalTitle()).toMatch(/Create or edit a Resultrecemption/);
        resultrecemptionDialogPage.close();
    });

    it('should create and save Resultrecemptions', () => {
        resultrecemptionComponentsPage.clickOnCreateButton();
        resultrecemptionDialogPage.setDateInput(12310020012301);
        expect(resultrecemptionDialogPage.getDateInput()).toMatch('2001-12-31T02:30');
        resultrecemptionDialogPage.setDiagnosisInput('diagnosis');
        expect(resultrecemptionDialogPage.getDiagnosisInput()).toMatch('diagnosis');
        resultrecemptionDialogPage.setProceduresInput('procedures');
        expect(resultrecemptionDialogPage.getProceduresInput()).toMatch('procedures');
        resultrecemptionDialogPage.setStatusInput('5');
        expect(resultrecemptionDialogPage.getStatusInput()).toMatch('5');
        resultrecemptionDialogPage.setTreatmentplanInput('treatmentplan');
        expect(resultrecemptionDialogPage.getTreatmentplanInput()).toMatch('treatmentplan');
        resultrecemptionDialogPage.resultcalendarreceptionSelectLastOption();
        // resultrecemptionDialogPage.userSelectLastOption();
        resultrecemptionDialogPage.save();
        expect(resultrecemptionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ResultrecemptionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-resultrecemption div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class ResultrecemptionDialogPage {
    modalTitle = element(by.css('h4#myResultrecemptionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateInput = element(by.css('input#field_date'));
    diagnosisInput = element(by.css('input#field_diagnosis'));
    proceduresInput = element(by.css('input#field_procedures'));
    statusInput = element(by.css('input#field_status'));
    treatmentplanInput = element(by.css('input#field_treatmentplan'));
    resultcalendarreceptionSelect = element(by.css('select#field_resultcalendarreception'));
    userSelect = element(by.css('select#field_user'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setDateInput = function (date) {
        this.dateInput.sendKeys(date);
    }

    getDateInput = function () {
        return this.dateInput.getAttribute('value');
    }

    setDiagnosisInput = function (diagnosis) {
        this.diagnosisInput.sendKeys(diagnosis);
    }

    getDiagnosisInput = function () {
        return this.diagnosisInput.getAttribute('value');
    }

    setProceduresInput = function (procedures) {
        this.proceduresInput.sendKeys(procedures);
    }

    getProceduresInput = function () {
        return this.proceduresInput.getAttribute('value');
    }

    setStatusInput = function (status) {
        this.statusInput.sendKeys(status);
    }

    getStatusInput = function () {
        return this.statusInput.getAttribute('value');
    }

    setTreatmentplanInput = function (treatmentplan) {
        this.treatmentplanInput.sendKeys(treatmentplan);
    }

    getTreatmentplanInput = function () {
        return this.treatmentplanInput.getAttribute('value');
    }

    resultcalendarreceptionSelectLastOption = function () {
        this.resultcalendarreceptionSelect.all(by.tagName('option')).last().click();
    }

    resultcalendarreceptionSelectOption = function (option) {
        this.resultcalendarreceptionSelect.sendKeys(option);
    }

    getResultcalendarreceptionSelect = function () {
        return this.resultcalendarreceptionSelect;
    }

    getResultcalendarreceptionSelectedOption = function () {
        return this.resultcalendarreceptionSelect.element(by.css('option:checked')).getText();
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}

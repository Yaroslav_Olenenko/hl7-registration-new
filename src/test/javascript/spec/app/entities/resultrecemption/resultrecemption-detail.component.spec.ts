/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { Hl7RegistrationTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ResultrecemptionDetailComponent } from '../../../../../../main/webapp/app/entities/resultrecemption/resultrecemption-detail.component';
import { ResultrecemptionService } from '../../../../../../main/webapp/app/entities/resultrecemption/resultrecemption.service';
import { Resultrecemption } from '../../../../../../main/webapp/app/entities/resultrecemption/resultrecemption.model';

describe('Component Tests', () => {

    describe('Resultrecemption Management Detail Component', () => {
        let comp: ResultrecemptionDetailComponent;
        let fixture: ComponentFixture<ResultrecemptionDetailComponent>;
        let service: ResultrecemptionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [Hl7RegistrationTestModule],
                declarations: [ResultrecemptionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ResultrecemptionService,
                    JhiEventManager
                ]
            }).overrideTemplate(ResultrecemptionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ResultrecemptionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ResultrecemptionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Resultrecemption(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.resultrecemption).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});

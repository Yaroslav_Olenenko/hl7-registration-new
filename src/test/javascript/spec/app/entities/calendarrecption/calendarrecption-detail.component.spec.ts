/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { Hl7RegistrationTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CalendarrecptionDetailComponent } from '../../../../../../main/webapp/app/entities/calendarrecption/calendarrecption-detail.component';
import { CalendarrecptionService } from '../../../../../../main/webapp/app/entities/calendarrecption/calendarrecption.service';
import { Calendarrecption } from '../../../../../../main/webapp/app/entities/calendarrecption/calendarrecption.model';

describe('Component Tests', () => {

    describe('Calendarrecption Management Detail Component', () => {
        let comp: CalendarrecptionDetailComponent;
        let fixture: ComponentFixture<CalendarrecptionDetailComponent>;
        let service: CalendarrecptionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [Hl7RegistrationTestModule],
                declarations: [CalendarrecptionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CalendarrecptionService,
                    JhiEventManager
                ]
            }).overrideTemplate(CalendarrecptionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CalendarrecptionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CalendarrecptionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Calendarrecption(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.calendarrecption).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});

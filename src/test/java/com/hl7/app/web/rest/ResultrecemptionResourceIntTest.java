package com.hl7.app.web.rest;

import com.hl7.app.Hl7RegistrationApp;

import com.hl7.app.domain.Resultrecemption;
import com.hl7.app.repository.ResultrecemptionRepository;
import com.hl7.app.repository.search.ResultrecemptionSearchRepository;
import com.hl7.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.hl7.app.web.rest.TestUtil.sameInstant;
import static com.hl7.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ResultrecemptionResource REST controller.
 *
 * @see ResultrecemptionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Hl7RegistrationApp.class)
public class ResultrecemptionResourceIntTest {

    private static final ZonedDateTime DEFAULT_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_DIAGNOSIS = "AAAAAAAAAA";
    private static final String UPDATED_DIAGNOSIS = "BBBBBBBBBB";

    private static final String DEFAULT_PROCEDURES = "AAAAAAAAAA";
    private static final String UPDATED_PROCEDURES = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_TREATMENTPLAN = "AAAAAAAAAA";
    private static final String UPDATED_TREATMENTPLAN = "BBBBBBBBBB";

    @Autowired
    private ResultrecemptionRepository resultrecemptionRepository;

    @Autowired
    private ResultrecemptionSearchRepository resultrecemptionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restResultrecemptionMockMvc;

    private Resultrecemption resultrecemption;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ResultrecemptionResource resultrecemptionResource = new ResultrecemptionResource(resultrecemptionRepository, resultrecemptionSearchRepository);
        this.restResultrecemptionMockMvc = MockMvcBuilders.standaloneSetup(resultrecemptionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Resultrecemption createEntity(EntityManager em) {
        Resultrecemption resultrecemption = new Resultrecemption()
            .date(DEFAULT_DATE)
            .diagnosis(DEFAULT_DIAGNOSIS)
            .procedures(DEFAULT_PROCEDURES)
            .status(DEFAULT_STATUS)
            .treatmentplan(DEFAULT_TREATMENTPLAN);
        return resultrecemption;
    }

    @Before
    public void initTest() {
        resultrecemptionSearchRepository.deleteAll();
        resultrecemption = createEntity(em);
    }

    @Test
    @Transactional
    public void createResultrecemption() throws Exception {
        int databaseSizeBeforeCreate = resultrecemptionRepository.findAll().size();

        // Create the Resultrecemption
        restResultrecemptionMockMvc.perform(post("/api/resultrecemptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultrecemption)))
            .andExpect(status().isCreated());

        // Validate the Resultrecemption in the database
        List<Resultrecemption> resultrecemptionList = resultrecemptionRepository.findAll();
        assertThat(resultrecemptionList).hasSize(databaseSizeBeforeCreate + 1);
        Resultrecemption testResultrecemption = resultrecemptionList.get(resultrecemptionList.size() - 1);
        assertThat(testResultrecemption.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testResultrecemption.getDiagnosis()).isEqualTo(DEFAULT_DIAGNOSIS);
        assertThat(testResultrecemption.getProcedures()).isEqualTo(DEFAULT_PROCEDURES);
        assertThat(testResultrecemption.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testResultrecemption.getTreatmentplan()).isEqualTo(DEFAULT_TREATMENTPLAN);

        // Validate the Resultrecemption in Elasticsearch
        Resultrecemption resultrecemptionEs = resultrecemptionSearchRepository.findOne(testResultrecemption.getId());
        assertThat(resultrecemptionEs).isEqualToComparingFieldByField(testResultrecemption);
    }

    @Test
    @Transactional
    public void createResultrecemptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resultrecemptionRepository.findAll().size();

        // Create the Resultrecemption with an existing ID
        resultrecemption.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResultrecemptionMockMvc.perform(post("/api/resultrecemptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultrecemption)))
            .andExpect(status().isBadRequest());

        // Validate the Resultrecemption in the database
        List<Resultrecemption> resultrecemptionList = resultrecemptionRepository.findAll();
        assertThat(resultrecemptionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDiagnosisIsRequired() throws Exception {
        int databaseSizeBeforeTest = resultrecemptionRepository.findAll().size();
        // set the field null
        resultrecemption.setDiagnosis(null);

        // Create the Resultrecemption, which fails.

        restResultrecemptionMockMvc.perform(post("/api/resultrecemptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultrecemption)))
            .andExpect(status().isBadRequest());

        List<Resultrecemption> resultrecemptionList = resultrecemptionRepository.findAll();
        assertThat(resultrecemptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = resultrecemptionRepository.findAll().size();
        // set the field null
        resultrecemption.setStatus(null);

        // Create the Resultrecemption, which fails.

        restResultrecemptionMockMvc.perform(post("/api/resultrecemptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultrecemption)))
            .andExpect(status().isBadRequest());

        List<Resultrecemption> resultrecemptionList = resultrecemptionRepository.findAll();
        assertThat(resultrecemptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllResultrecemptions() throws Exception {
        // Initialize the database
        resultrecemptionRepository.saveAndFlush(resultrecemption);

        // Get all the resultrecemptionList
        restResultrecemptionMockMvc.perform(get("/api/resultrecemptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resultrecemption.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))))
            .andExpect(jsonPath("$.[*].diagnosis").value(hasItem(DEFAULT_DIAGNOSIS.toString())))
            .andExpect(jsonPath("$.[*].procedures").value(hasItem(DEFAULT_PROCEDURES.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].treatmentplan").value(hasItem(DEFAULT_TREATMENTPLAN.toString())));
    }

    @Test
    @Transactional
    public void getResultrecemption() throws Exception {
        // Initialize the database
        resultrecemptionRepository.saveAndFlush(resultrecemption);

        // Get the resultrecemption
        restResultrecemptionMockMvc.perform(get("/api/resultrecemptions/{id}", resultrecemption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resultrecemption.getId().intValue()))
            .andExpect(jsonPath("$.date").value(sameInstant(DEFAULT_DATE)))
            .andExpect(jsonPath("$.diagnosis").value(DEFAULT_DIAGNOSIS.toString()))
            .andExpect(jsonPath("$.procedures").value(DEFAULT_PROCEDURES.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.treatmentplan").value(DEFAULT_TREATMENTPLAN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingResultrecemption() throws Exception {
        // Get the resultrecemption
        restResultrecemptionMockMvc.perform(get("/api/resultrecemptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResultrecemption() throws Exception {
        // Initialize the database
        resultrecemptionRepository.saveAndFlush(resultrecemption);
        resultrecemptionSearchRepository.save(resultrecemption);
        int databaseSizeBeforeUpdate = resultrecemptionRepository.findAll().size();

        // Update the resultrecemption
        Resultrecemption updatedResultrecemption = resultrecemptionRepository.findOne(resultrecemption.getId());
        // Disconnect from session so that the updates on updatedResultrecemption are not directly saved in db
        em.detach(updatedResultrecemption);
        updatedResultrecemption
            .date(UPDATED_DATE)
            .diagnosis(UPDATED_DIAGNOSIS)
            .procedures(UPDATED_PROCEDURES)
            .status(UPDATED_STATUS)
            .treatmentplan(UPDATED_TREATMENTPLAN);

        restResultrecemptionMockMvc.perform(put("/api/resultrecemptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedResultrecemption)))
            .andExpect(status().isOk());

        // Validate the Resultrecemption in the database
        List<Resultrecemption> resultrecemptionList = resultrecemptionRepository.findAll();
        assertThat(resultrecemptionList).hasSize(databaseSizeBeforeUpdate);
        Resultrecemption testResultrecemption = resultrecemptionList.get(resultrecemptionList.size() - 1);
        assertThat(testResultrecemption.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testResultrecemption.getDiagnosis()).isEqualTo(UPDATED_DIAGNOSIS);
        assertThat(testResultrecemption.getProcedures()).isEqualTo(UPDATED_PROCEDURES);
        assertThat(testResultrecemption.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testResultrecemption.getTreatmentplan()).isEqualTo(UPDATED_TREATMENTPLAN);

        // Validate the Resultrecemption in Elasticsearch
        Resultrecemption resultrecemptionEs = resultrecemptionSearchRepository.findOne(testResultrecemption.getId());
        assertThat(resultrecemptionEs).isEqualToComparingFieldByField(testResultrecemption);
    }

    @Test
    @Transactional
    public void updateNonExistingResultrecemption() throws Exception {
        int databaseSizeBeforeUpdate = resultrecemptionRepository.findAll().size();

        // Create the Resultrecemption

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restResultrecemptionMockMvc.perform(put("/api/resultrecemptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultrecemption)))
            .andExpect(status().isCreated());

        // Validate the Resultrecemption in the database
        List<Resultrecemption> resultrecemptionList = resultrecemptionRepository.findAll();
        assertThat(resultrecemptionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteResultrecemption() throws Exception {
        // Initialize the database
        resultrecemptionRepository.saveAndFlush(resultrecemption);
        resultrecemptionSearchRepository.save(resultrecemption);
        int databaseSizeBeforeDelete = resultrecemptionRepository.findAll().size();

        // Get the resultrecemption
        restResultrecemptionMockMvc.perform(delete("/api/resultrecemptions/{id}", resultrecemption.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean resultrecemptionExistsInEs = resultrecemptionSearchRepository.exists(resultrecemption.getId());
        assertThat(resultrecemptionExistsInEs).isFalse();

        // Validate the database is empty
        List<Resultrecemption> resultrecemptionList = resultrecemptionRepository.findAll();
        assertThat(resultrecemptionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchResultrecemption() throws Exception {
        // Initialize the database
        resultrecemptionRepository.saveAndFlush(resultrecemption);
        resultrecemptionSearchRepository.save(resultrecemption);

        // Search the resultrecemption
        restResultrecemptionMockMvc.perform(get("/api/_search/resultrecemptions?query=id:" + resultrecemption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resultrecemption.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))))
            .andExpect(jsonPath("$.[*].diagnosis").value(hasItem(DEFAULT_DIAGNOSIS.toString())))
            .andExpect(jsonPath("$.[*].procedures").value(hasItem(DEFAULT_PROCEDURES.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].treatmentplan").value(hasItem(DEFAULT_TREATMENTPLAN.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Resultrecemption.class);
        Resultrecemption resultrecemption1 = new Resultrecemption();
        resultrecemption1.setId(1L);
        Resultrecemption resultrecemption2 = new Resultrecemption();
        resultrecemption2.setId(resultrecemption1.getId());
        assertThat(resultrecemption1).isEqualTo(resultrecemption2);
        resultrecemption2.setId(2L);
        assertThat(resultrecemption1).isNotEqualTo(resultrecemption2);
        resultrecemption1.setId(null);
        assertThat(resultrecemption1).isNotEqualTo(resultrecemption2);
    }
}

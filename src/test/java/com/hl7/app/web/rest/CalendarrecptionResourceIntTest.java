package com.hl7.app.web.rest;

import com.hl7.app.Hl7RegistrationApp;

import com.hl7.app.domain.Calendarrecption;
import com.hl7.app.repository.CalendarrecptionRepository;
import com.hl7.app.repository.search.CalendarrecptionSearchRepository;
import com.hl7.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.hl7.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CalendarrecptionResource REST controller.
 *
 * @see CalendarrecptionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Hl7RegistrationApp.class)
public class CalendarrecptionResourceIntTest {

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_ADRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADRESS = "BBBBBBBBBB";

    @Autowired
    private CalendarrecptionRepository calendarrecptionRepository;

    @Autowired
    private CalendarrecptionSearchRepository calendarrecptionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCalendarrecptionMockMvc;

    private Calendarrecption calendarrecption;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CalendarrecptionResource calendarrecptionResource = new CalendarrecptionResource(calendarrecptionRepository, calendarrecptionSearchRepository);
        this.restCalendarrecptionMockMvc = MockMvcBuilders.standaloneSetup(calendarrecptionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Calendarrecption createEntity(EntityManager em) {
        Calendarrecption calendarrecption = new Calendarrecption()
            .date(DEFAULT_DATE)
            .adress(DEFAULT_ADRESS);
        return calendarrecption;
    }

    @Before
    public void initTest() {
        calendarrecptionSearchRepository.deleteAll();
        calendarrecption = createEntity(em);
    }

    @Test
    @Transactional
    public void createCalendarrecption() throws Exception {
        int databaseSizeBeforeCreate = calendarrecptionRepository.findAll().size();

        // Create the Calendarrecption
        restCalendarrecptionMockMvc.perform(post("/api/calendarrecptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calendarrecption)))
            .andExpect(status().isCreated());

        // Validate the Calendarrecption in the database
        List<Calendarrecption> calendarrecptionList = calendarrecptionRepository.findAll();
        assertThat(calendarrecptionList).hasSize(databaseSizeBeforeCreate + 1);
        Calendarrecption testCalendarrecption = calendarrecptionList.get(calendarrecptionList.size() - 1);
        assertThat(testCalendarrecption.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testCalendarrecption.getAdress()).isEqualTo(DEFAULT_ADRESS);

        // Validate the Calendarrecption in Elasticsearch
        Calendarrecption calendarrecptionEs = calendarrecptionSearchRepository.findOne(testCalendarrecption.getId());
        assertThat(calendarrecptionEs).isEqualToComparingFieldByField(testCalendarrecption);
    }

    @Test
    @Transactional
    public void createCalendarrecptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = calendarrecptionRepository.findAll().size();

        // Create the Calendarrecption with an existing ID
        calendarrecption.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCalendarrecptionMockMvc.perform(post("/api/calendarrecptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calendarrecption)))
            .andExpect(status().isBadRequest());

        // Validate the Calendarrecption in the database
        List<Calendarrecption> calendarrecptionList = calendarrecptionRepository.findAll();
        assertThat(calendarrecptionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = calendarrecptionRepository.findAll().size();
        // set the field null
        calendarrecption.setDate(null);

        // Create the Calendarrecption, which fails.

        restCalendarrecptionMockMvc.perform(post("/api/calendarrecptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calendarrecption)))
            .andExpect(status().isBadRequest());

        List<Calendarrecption> calendarrecptionList = calendarrecptionRepository.findAll();
        assertThat(calendarrecptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAdressIsRequired() throws Exception {
        int databaseSizeBeforeTest = calendarrecptionRepository.findAll().size();
        // set the field null
        calendarrecption.setAdress(null);

        // Create the Calendarrecption, which fails.

        restCalendarrecptionMockMvc.perform(post("/api/calendarrecptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calendarrecption)))
            .andExpect(status().isBadRequest());

        List<Calendarrecption> calendarrecptionList = calendarrecptionRepository.findAll();
        assertThat(calendarrecptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCalendarrecptions() throws Exception {
        // Initialize the database
        calendarrecptionRepository.saveAndFlush(calendarrecption);

        // Get all the calendarrecptionList
        restCalendarrecptionMockMvc.perform(get("/api/calendarrecptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(calendarrecption.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS.toString())));
    }

    @Test
    @Transactional
    public void getCalendarrecption() throws Exception {
        // Initialize the database
        calendarrecptionRepository.saveAndFlush(calendarrecption);

        // Get the calendarrecption
        restCalendarrecptionMockMvc.perform(get("/api/calendarrecptions/{id}", calendarrecption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(calendarrecption.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.adress").value(DEFAULT_ADRESS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCalendarrecption() throws Exception {
        // Get the calendarrecption
        restCalendarrecptionMockMvc.perform(get("/api/calendarrecptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCalendarrecption() throws Exception {
        // Initialize the database
        calendarrecptionRepository.saveAndFlush(calendarrecption);
        calendarrecptionSearchRepository.save(calendarrecption);
        int databaseSizeBeforeUpdate = calendarrecptionRepository.findAll().size();

        // Update the calendarrecption
        Calendarrecption updatedCalendarrecption = calendarrecptionRepository.findOne(calendarrecption.getId());
        // Disconnect from session so that the updates on updatedCalendarrecption are not directly saved in db
        em.detach(updatedCalendarrecption);
        updatedCalendarrecption
            .date(UPDATED_DATE)
            .adress(UPDATED_ADRESS);

        restCalendarrecptionMockMvc.perform(put("/api/calendarrecptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCalendarrecption)))
            .andExpect(status().isOk());

        // Validate the Calendarrecption in the database
        List<Calendarrecption> calendarrecptionList = calendarrecptionRepository.findAll();
        assertThat(calendarrecptionList).hasSize(databaseSizeBeforeUpdate);
        Calendarrecption testCalendarrecption = calendarrecptionList.get(calendarrecptionList.size() - 1);
        assertThat(testCalendarrecption.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCalendarrecption.getAdress()).isEqualTo(UPDATED_ADRESS);

        // Validate the Calendarrecption in Elasticsearch
        Calendarrecption calendarrecptionEs = calendarrecptionSearchRepository.findOne(testCalendarrecption.getId());
        assertThat(calendarrecptionEs).isEqualToComparingFieldByField(testCalendarrecption);
    }

    @Test
    @Transactional
    public void updateNonExistingCalendarrecption() throws Exception {
        int databaseSizeBeforeUpdate = calendarrecptionRepository.findAll().size();

        // Create the Calendarrecption

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCalendarrecptionMockMvc.perform(put("/api/calendarrecptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(calendarrecption)))
            .andExpect(status().isCreated());

        // Validate the Calendarrecption in the database
        List<Calendarrecption> calendarrecptionList = calendarrecptionRepository.findAll();
        assertThat(calendarrecptionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCalendarrecption() throws Exception {
        // Initialize the database
        calendarrecptionRepository.saveAndFlush(calendarrecption);
        calendarrecptionSearchRepository.save(calendarrecption);
        int databaseSizeBeforeDelete = calendarrecptionRepository.findAll().size();

        // Get the calendarrecption
        restCalendarrecptionMockMvc.perform(delete("/api/calendarrecptions/{id}", calendarrecption.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean calendarrecptionExistsInEs = calendarrecptionSearchRepository.exists(calendarrecption.getId());
        assertThat(calendarrecptionExistsInEs).isFalse();

        // Validate the database is empty
        List<Calendarrecption> calendarrecptionList = calendarrecptionRepository.findAll();
        assertThat(calendarrecptionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCalendarrecption() throws Exception {
        // Initialize the database
        calendarrecptionRepository.saveAndFlush(calendarrecption);
        calendarrecptionSearchRepository.save(calendarrecption);

        // Search the calendarrecption
        restCalendarrecptionMockMvc.perform(get("/api/_search/calendarrecptions?query=id:" + calendarrecption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(calendarrecption.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Calendarrecption.class);
        Calendarrecption calendarrecption1 = new Calendarrecption();
        calendarrecption1.setId(1L);
        Calendarrecption calendarrecption2 = new Calendarrecption();
        calendarrecption2.setId(calendarrecption1.getId());
        assertThat(calendarrecption1).isEqualTo(calendarrecption2);
        calendarrecption2.setId(2L);
        assertThat(calendarrecption1).isNotEqualTo(calendarrecption2);
        calendarrecption1.setId(null);
        assertThat(calendarrecption1).isNotEqualTo(calendarrecption2);
    }
}

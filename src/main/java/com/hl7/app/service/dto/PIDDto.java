package com.hl7.app.service.dto;

import com.hl7.app.hl7_processor.HL7Field;
import com.hl7.app.security.AuthoritiesConstants;

import java.time.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class PIDDto {

    private ArrayList<HL7Field> fields;

    public PIDDto(ArrayList<HL7Field> fields) {
        this.fields = fields;
    }


    public String getLogin() {
        return getFirstName().toLowerCase() + getLastName().toLowerCase();
    }

    // Given Name
    public String getFirstName() {
        return this.fields.get(5).get(0, 1, 0);
    }


    // Family Name
    public String getLastName() {
        return this.fields.get(5).get(0, 0, 0); //
    }

    public String getEmail() {
        return getLogin().concat("@gmail");
    }

    public Set<String> getAuthority() {
        Set <String> authority = new HashSet<>();
        authority.add(AuthoritiesConstants.USER);

        return authority;
    }

    public LocalDateTime getBirthDate() {
        String time = this.fields.get(7).get(0, 0, 0);
        LocalDateTime birthDate;

        if (time.isEmpty()) {
            birthDate = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault());
        } else {
            birthDate = Instant.ofEpochMilli(Long.valueOf(time)).atZone(ZoneId.systemDefault()).toLocalDateTime();
        }

        return birthDate;
    }

    public Long getGender() {
        String gender = this.fields.get(8).get(0,0,0);
        Long result = 0L;

        if (!gender.isEmpty()) {
            if (gender.equals("F")) {
                result = 1L;
            }

            if (gender.equals("M")) {
                result = 2L;
            }

            if (gender.equals("N")) {
                result = 3L;
            }

            if (gender.equals("O")) {
                result = 4L;
            }

            if (gender.equals("U")) {
                result = 5L;
            }

            if (gender.equals("A")) {
                result = 6L;
            }
        }

        return result;
    }

    public Long getCountry() {
        Long result = 0L;
        String countryCode = this.fields.get(11).get(0,5,0);

        if (!countryCode.isEmpty()) {
            if (countryCode.equals("UA")) {
                result = 1L;
            }

            if (countryCode.equals("USA")) {
                result = 2L;
            }

            if (countryCode.equals("RUS")) {
                result = 3L;
            }

            if (countryCode.equals("GER")) {
                result = 4L;
            }

            if (countryCode.equals("UK")) {
                result = 5L;
            }
        }

        return result;
    }

    public UserDTO toUserDTO() {
        UserDTO user = new UserDTO();
        user.setGender(getGender());
        user.setBirthDate(getBirthDate());
        user.setCountry(getCountry());
        user.setAuthorities(getAuthority());
        user.setLogin(getLogin());
        user.setFirstName(getFirstName());
        user.setLastName(getLastName());
        user.setEmail(getEmail());
        user.setStatus(0L);

        return user;
    }
}

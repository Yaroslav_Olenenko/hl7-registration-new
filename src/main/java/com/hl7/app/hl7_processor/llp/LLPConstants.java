
/*
 * Copyright (C) 2008 Archie L. Cobbs. All rights reserved.
 */

package com.hl7.app.hl7_processor.llp;

/**
 * HL7 LLP framing constants.
 */
public final class LLPConstants {

    public static final int LEADING_BYTE = 0x0b;
    public static final int TRAILING_BYTE_0 = 0x1c;
    public static final int TRAILING_BYTE_1 = 0x0d;

    private LLPConstants() {
    }
}


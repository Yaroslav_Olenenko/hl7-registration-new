
/*
 * Copyright (C) 2011 Archie L. Cobbs. All rights reserved.
 */

/**
 * Classes relating to HL7 "lower layer protocol" (LLP) framing.
 *
 * @see <a href="https://github.com/archiecobbs/hl7lib">The hl7lib Project</a>
 */
package com.hl7.app.hl7_processor.llp;

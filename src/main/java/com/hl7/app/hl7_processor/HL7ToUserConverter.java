package com.hl7.app.hl7_processor;

import com.hl7.app.service.dto.PIDDto;
import com.hl7.app.service.dto.UserDTO;

import java.util.ArrayList;

public class HL7ToUserConverter {

    private HL7Message message;

    public HL7ToUserConverter (HL7Message message) {
        this.message = message;
    }

    public UserDTO getUserRegistrationInfo () {
        HL7Segment segment  = getSegmentByName("PID");
        ArrayList<HL7Field> fields = segment.fields;
        PIDDto pidDto = new PIDDto(fields);

        return pidDto.toUserDTO();
    }

    private HL7Segment getSegmentByName(String name) {
        HL7Segment segment = null;

        for (HL7Segment segmentIterator: this.message.segments) {
            if (name.equals(segmentIterator.fields.get(0).value[0][0][0])) {
                segment = segmentIterator;
                break;
            }
        }

        return segment;
    }
}

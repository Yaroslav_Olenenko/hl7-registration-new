package com.hl7.app.hl7_processor.io;

import com.hl7.app.hl7_processor.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class HL7MessageReader implements HL7Reader {

    private String nextLine;
    private Scanner scanner;
    private boolean closed;

    /**
     * Constructor.
     *
     * @param in underlying reader
     */
    public HL7MessageReader(String in) {
        if (in == null)
            throw new IllegalArgumentException("null in");
        scanner = new Scanner(in);
    }

    /**
     * Read next message from the underlying stream.
     *
     * @throws EOFException if there is no more input
     * @throws HL7ContentException if a malformed message is read
     * @throws IOException if an error occurs on the underlying stream
     */
    public HL7Message readMessage() throws IOException, HL7ContentException {
        String line = nextLine();
        if (line == null)
            throw new EOFException();
        MSHSegment msh = new MSHSegment(line);
        HL7Seps seps = msh.getHL7Seps();
        HL7Message message = new HL7Message(msh);
        while ((line = nextLine()) != null) {
            if (line.startsWith(MSHSegment.MSH_SEGMENT_NAME)) {
                this.pushback(line);
                break;
            }
            message.getSegments().add(new HL7Segment(line, seps));
        }
        return message;
    }

    /**
     * Save the line for next time.
     *
     * @param line line to push back
     * @throws RuntimeException if there is already one saved
     */
    protected void pushback(String line) {
        if (this.nextLine != null)
            throw new RuntimeException("internal error");
        this.nextLine = line;
    }

    /**
     * Read next non-ignorable line.
     *
     * @return next relevant line, or null for EOF
     * @throws IOException if an I/O error occurs
     */
    protected String nextLine() throws IOException {
        String line = this.nextLine;
        this.nextLine = null;
        if (line != null)
            return line;
        while (true) {
            if (this.scanner.hasNextLine()) {
                line = this.scanner.nextLine();
            } else {
                this.scanner.close();
                line = null;
            }

            if (line == null)
                return null;
            if (line.length() == 0 || line.charAt(0) == '#')
                continue;
            if (line.matches("\\s+"))
                continue;
            return line;
        }
    }

    @Override
    public void close() throws IOException {
        this.scanner.close();
        this.nextLine = null;
        this.closed = true;
    }
}

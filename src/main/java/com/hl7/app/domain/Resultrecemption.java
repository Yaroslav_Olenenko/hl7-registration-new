package com.hl7.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A Resultrecemption.
 */
@Entity
@Table(name = "resultrecemption")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "resultrecemption")
public class Resultrecemption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jhi_date")
    private ZonedDateTime date;

    @NotNull
    @Column(name = "diagnosis", nullable = false)
    private String diagnosis;

    @Column(name = "procedures")
    private String procedures;

    @NotNull
    @Column(name = "status", nullable = false)
    private Integer status;

    @Column(name = "treatmentplan")
    private String treatmentplan;

    @OneToOne
    @JoinColumn(unique = true)
    private Calendarrecption resultcalendarreception;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "resultrecemption_user",
               joinColumns = @JoinColumn(name="resultrecemptions_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="users_id", referencedColumnName="id"))
    private Set<User> users = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public Resultrecemption date(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public Resultrecemption diagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
        return this;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getProcedures() {
        return procedures;
    }

    public Resultrecemption procedures(String procedures) {
        this.procedures = procedures;
        return this;
    }

    public void setProcedures(String procedures) {
        this.procedures = procedures;
    }

    public Integer getStatus() {
        return status;
    }

    public Resultrecemption status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTreatmentplan() {
        return treatmentplan;
    }

    public Resultrecemption treatmentplan(String treatmentplan) {
        this.treatmentplan = treatmentplan;
        return this;
    }

    public void setTreatmentplan(String treatmentplan) {
        this.treatmentplan = treatmentplan;
    }

    public Calendarrecption getResultcalendarreception() {
        return resultcalendarreception;
    }

    public Resultrecemption resultcalendarreception(Calendarrecption calendarrecption) {
        this.resultcalendarreception = calendarrecption;
        return this;
    }

    public void setResultcalendarreception(Calendarrecption calendarrecption) {
        this.resultcalendarreception = calendarrecption;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Resultrecemption users(Set<User> users) {
        this.users = users;
        return this;
    }

    public Resultrecemption addUser(User user) {
        this.users.add(user);
      //  user.getResultrecemptions().add(this);
        return this;
    }

    public Resultrecemption removeUser(User user) {
        this.users.remove(user);
    //    user.getResultrecemptions().remove(this);
        return this;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Resultrecemption resultrecemption = (Resultrecemption) o;
        if (resultrecemption.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), resultrecemption.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Resultrecemption{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", diagnosis='" + getDiagnosis() + "'" +
            ", procedures='" + getProcedures() + "'" +
            ", status=" + getStatus() +
            ", treatmentplan='" + getTreatmentplan() + "'" +
            "}";
    }
}

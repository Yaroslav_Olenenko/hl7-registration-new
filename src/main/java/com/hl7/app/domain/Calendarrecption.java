package com.hl7.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A Calendarrecption.
 */
@Entity
@Table(name = "calendarrecption")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "calendarrecption")
public class Calendarrecption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "jhi_date", nullable = false)
    private LocalDate date;

    @NotNull
    @Size(min = 5)
    @Column(name = "adress", nullable = false)
    private String adress;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "calendarrecption_calendarreceptionuser",
               joinColumns = @JoinColumn(name="calendarrecptions_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="calendarreceptionusers_id", referencedColumnName="id"))
    private Set<User> calendarreceptionusers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Calendarrecption date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getAdress() {
        return adress;
    }

    public Calendarrecption adress(String adress) {
        this.adress = adress;
        return this;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Set<User> getCalendarreceptionusers() {
        return calendarreceptionusers;
    }

    public Calendarrecption calendarreceptionusers(Set<User> users) {
        this.calendarreceptionusers = users;
        return this;
    }

    public Calendarrecption addCalendarreceptionuser(User user) {
        this.calendarreceptionusers.add(user);
        user.getCalendarrecptions().add(this);
        return this;
    }

    public Calendarrecption removeCalendarreceptionuser(User user) {
        this.calendarreceptionusers.remove(user);
        user.getCalendarrecptions().remove(this);
        return this;
    }

    public void setCalendarreceptionusers(Set<User> users) {
        this.calendarreceptionusers = users;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Calendarrecption calendarrecption = (Calendarrecption) o;
        if (calendarrecption.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), calendarrecption.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Calendarrecption{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", adress='" + getAdress() + "'" +
            "}";
    }
}

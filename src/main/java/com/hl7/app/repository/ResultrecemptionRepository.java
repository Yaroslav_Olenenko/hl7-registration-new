package com.hl7.app.repository;

import com.hl7.app.domain.Resultrecemption;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Resultrecemption entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResultrecemptionRepository extends JpaRepository<Resultrecemption, Long> {
    @Query("select distinct resultrecemption from Resultrecemption resultrecemption left join fetch resultrecemption.users")
    List<Resultrecemption> findAllWithEagerRelationships();

    @Query("select resultrecemption from Resultrecemption resultrecemption left join fetch resultrecemption.users where resultrecemption.id =:id")
    Resultrecemption findOneWithEagerRelationships(@Param("id") Long id);

}

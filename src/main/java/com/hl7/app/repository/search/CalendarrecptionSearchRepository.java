package com.hl7.app.repository.search;

import com.hl7.app.domain.Calendarrecption;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Calendarrecption entity.
 */
public interface CalendarrecptionSearchRepository extends ElasticsearchRepository<Calendarrecption, Long> {
}

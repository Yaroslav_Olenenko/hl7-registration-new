package com.hl7.app.repository.search;

import com.hl7.app.domain.Roles;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Roles entity.
 */
public interface RolesSearchRepository extends ElasticsearchRepository<Roles, Long> {
}

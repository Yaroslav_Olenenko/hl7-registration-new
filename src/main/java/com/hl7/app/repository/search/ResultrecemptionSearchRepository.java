package com.hl7.app.repository.search;

import com.hl7.app.domain.Resultrecemption;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Resultrecemption entity.
 */
public interface ResultrecemptionSearchRepository extends ElasticsearchRepository<Resultrecemption, Long> {
}

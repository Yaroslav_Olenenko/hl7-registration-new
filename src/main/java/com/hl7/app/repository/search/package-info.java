/**
 * Spring Data Elasticsearch repositories.
 */
package com.hl7.app.repository.search;

package com.hl7.app.repository;

import com.hl7.app.domain.Roles;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Roles entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RolesRepository extends JpaRepository<Roles, Long> {
    @Query("select distinct roles from Roles roles left join fetch roles.userToRoles")
    List<Roles> findAllWithEagerRelationships();

    @Query("select roles from Roles roles left join fetch roles.userToRoles where roles.id =:id")
    Roles findOneWithEagerRelationships(@Param("id") Long id);

}

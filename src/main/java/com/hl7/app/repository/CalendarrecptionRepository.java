package com.hl7.app.repository;

import com.hl7.app.domain.Calendarrecption;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Calendarrecption entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CalendarrecptionRepository extends JpaRepository<Calendarrecption, Long> {
    @Query("select distinct calendarrecption from Calendarrecption calendarrecption left join fetch calendarrecption.calendarreceptionusers")
    List<Calendarrecption> findAllWithEagerRelationships();

    @Query("select calendarrecption from Calendarrecption calendarrecption left join fetch calendarrecption.calendarreceptionusers where calendarrecption.id =:id")
    Calendarrecption findOneWithEagerRelationships(@Param("id") Long id);

}

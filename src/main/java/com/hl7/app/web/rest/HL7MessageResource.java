package com.hl7.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hl7.app.domain.Authority;
import com.hl7.app.hl7_processor.HL7ContentException;
import com.hl7.app.hl7_processor.HL7Message;
import com.hl7.app.hl7_processor.HL7Reader;
import com.hl7.app.hl7_processor.HL7ToUserConverter;
import com.hl7.app.hl7_processor.io.HL7MessageReader;
import com.hl7.app.repository.AuthorityRepository;
import com.hl7.app.security.AuthoritiesConstants;
import com.hl7.app.service.UserService;
import com.hl7.app.service.dto.UserDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * REST controller for managing HL7 Message.
 */
@RestController
@RequestMapping("/api")
public class HL7MessageResource {

    private final UserService userService;

    HL7MessageResource(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/hl7/create-user")
    @Timed
    public void createNewUser(@RequestBody String message) {
        HL7Reader reader = new HL7MessageReader(message);
        try {
            HL7Message hl7message = reader.readMessage();
            HL7ToUserConverter userConverter = new HL7ToUserConverter(hl7message);
            UserDTO userDTO = userConverter.getUserRegistrationInfo();
            this.userService.createUser(userDTO);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (HL7ContentException e) {
            e.printStackTrace();
        }
    }
}

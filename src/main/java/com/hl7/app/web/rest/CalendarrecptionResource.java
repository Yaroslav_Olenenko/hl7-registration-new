package com.hl7.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hl7.app.domain.Calendarrecption;

import com.hl7.app.repository.CalendarrecptionRepository;
import com.hl7.app.repository.search.CalendarrecptionSearchRepository;
import com.hl7.app.web.rest.errors.BadRequestAlertException;
import com.hl7.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Calendarrecption.
 */
@RestController
@RequestMapping("/api")
public class CalendarrecptionResource {

    private final Logger log = LoggerFactory.getLogger(CalendarrecptionResource.class);

    private static final String ENTITY_NAME = "calendarrecption";

    private final CalendarrecptionRepository calendarrecptionRepository;

    private final CalendarrecptionSearchRepository calendarrecptionSearchRepository;

    public CalendarrecptionResource(CalendarrecptionRepository calendarrecptionRepository, CalendarrecptionSearchRepository calendarrecptionSearchRepository) {
        this.calendarrecptionRepository = calendarrecptionRepository;
        this.calendarrecptionSearchRepository = calendarrecptionSearchRepository;
    }

    /**
     * POST  /calendarrecptions : Create a new calendarrecption.
     *
     * @param calendarrecption the calendarrecption to create
     * @return the ResponseEntity with status 201 (Created) and with body the new calendarrecption, or with status 400 (Bad Request) if the calendarrecption has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/calendarrecptions")
    @Timed
    public ResponseEntity<Calendarrecption> createCalendarrecption(@Valid @RequestBody Calendarrecption calendarrecption) throws URISyntaxException {
        log.debug("REST request to save Calendarrecption : {}", calendarrecption);
        if (calendarrecption.getId() != null) {
            throw new BadRequestAlertException("A new calendarrecption cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Calendarrecption result = calendarrecptionRepository.save(calendarrecption);
        calendarrecptionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/calendarrecptions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /calendarrecptions : Updates an existing calendarrecption.
     *
     * @param calendarrecption the calendarrecption to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated calendarrecption,
     * or with status 400 (Bad Request) if the calendarrecption is not valid,
     * or with status 500 (Internal Server Error) if the calendarrecption couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/calendarrecptions")
    @Timed
    public ResponseEntity<Calendarrecption> updateCalendarrecption(@Valid @RequestBody Calendarrecption calendarrecption) throws URISyntaxException {
        log.debug("REST request to update Calendarrecption : {}", calendarrecption);
        if (calendarrecption.getId() == null) {
            return createCalendarrecption(calendarrecption);
        }
        Calendarrecption result = calendarrecptionRepository.save(calendarrecption);
        calendarrecptionSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, calendarrecption.getId().toString()))
            .body(result);
    }

    /**
     * GET  /calendarrecptions : get all the calendarrecptions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of calendarrecptions in body
     */
    @GetMapping("/calendarrecptions")
    @Timed
    public List<Calendarrecption> getAllCalendarrecptions() {
        log.debug("REST request to get all Calendarrecptions");
        return calendarrecptionRepository.findAllWithEagerRelationships();
        }

    /**
     * GET  /calendarrecptions/:id : get the "id" calendarrecption.
     *
     * @param id the id of the calendarrecption to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the calendarrecption, or with status 404 (Not Found)
     */
    @GetMapping("/calendarrecptions/{id}")
    @Timed
    public ResponseEntity<Calendarrecption> getCalendarrecption(@PathVariable Long id) {
        log.debug("REST request to get Calendarrecption : {}", id);
        Calendarrecption calendarrecption = calendarrecptionRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(calendarrecption));
    }

    /**
     * DELETE  /calendarrecptions/:id : delete the "id" calendarrecption.
     *
     * @param id the id of the calendarrecption to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/calendarrecptions/{id}")
    @Timed
    public ResponseEntity<Void> deleteCalendarrecption(@PathVariable Long id) {
        log.debug("REST request to delete Calendarrecption : {}", id);
        calendarrecptionRepository.delete(id);
        calendarrecptionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/calendarrecptions?query=:query : search for the calendarrecption corresponding
     * to the query.
     *
     * @param query the query of the calendarrecption search
     * @return the result of the search
     */
    @GetMapping("/_search/calendarrecptions")
    @Timed
    public List<Calendarrecption> searchCalendarrecptions(@RequestParam String query) {
        log.debug("REST request to search Calendarrecptions for query {}", query);
        return StreamSupport
            .stream(calendarrecptionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}

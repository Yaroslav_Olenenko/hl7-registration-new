package com.hl7.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hl7.app.domain.Resultrecemption;

import com.hl7.app.repository.ResultrecemptionRepository;
import com.hl7.app.repository.search.ResultrecemptionSearchRepository;
import com.hl7.app.web.rest.errors.BadRequestAlertException;
import com.hl7.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Resultrecemption.
 */
@RestController
@RequestMapping("/api")
public class ResultrecemptionResource {

    private final Logger log = LoggerFactory.getLogger(ResultrecemptionResource.class);

    private static final String ENTITY_NAME = "resultrecemption";

    private final ResultrecemptionRepository resultrecemptionRepository;

    private final ResultrecemptionSearchRepository resultrecemptionSearchRepository;

    public ResultrecemptionResource(ResultrecemptionRepository resultrecemptionRepository, ResultrecemptionSearchRepository resultrecemptionSearchRepository) {
        this.resultrecemptionRepository = resultrecemptionRepository;
        this.resultrecemptionSearchRepository = resultrecemptionSearchRepository;
    }

    /**
     * POST  /resultrecemptions : Create a new resultrecemption.
     *
     * @param resultrecemption the resultrecemption to create
     * @return the ResponseEntity with status 201 (Created) and with body the new resultrecemption, or with status 400 (Bad Request) if the resultrecemption has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/resultrecemptions")
    @Timed
    public ResponseEntity<Resultrecemption> createResultrecemption(@Valid @RequestBody Resultrecemption resultrecemption) throws URISyntaxException {
        log.debug("REST request to save Resultrecemption : {}", resultrecemption);
        if (resultrecemption.getId() != null) {
            throw new BadRequestAlertException("A new resultrecemption cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Resultrecemption result = resultrecemptionRepository.save(resultrecemption);
        resultrecemptionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/resultrecemptions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /resultrecemptions : Updates an existing resultrecemption.
     *
     * @param resultrecemption the resultrecemption to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated resultrecemption,
     * or with status 400 (Bad Request) if the resultrecemption is not valid,
     * or with status 500 (Internal Server Error) if the resultrecemption couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/resultrecemptions")
    @Timed
    public ResponseEntity<Resultrecemption> updateResultrecemption(@Valid @RequestBody Resultrecemption resultrecemption) throws URISyntaxException {
        log.debug("REST request to update Resultrecemption : {}", resultrecemption);
        if (resultrecemption.getId() == null) {
            return createResultrecemption(resultrecemption);
        }
        Resultrecemption result = resultrecemptionRepository.save(resultrecemption);
        resultrecemptionSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resultrecemption.getId().toString()))
            .body(result);
    }

    /**
     * GET  /resultrecemptions : get all the resultrecemptions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of resultrecemptions in body
     */
    @GetMapping("/resultrecemptions")
    @Timed
    public List<Resultrecemption> getAllResultrecemptions() {
        log.debug("REST request to get all Resultrecemptions");
        return resultrecemptionRepository.findAllWithEagerRelationships();
        }

    /**
     * GET  /resultrecemptions/:id : get the "id" resultrecemption.
     *
     * @param id the id of the resultrecemption to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the resultrecemption, or with status 404 (Not Found)
     */
    @GetMapping("/resultrecemptions/{id}")
    @Timed
    public ResponseEntity<Resultrecemption> getResultrecemption(@PathVariable Long id) {
        log.debug("REST request to get Resultrecemption : {}", id);
        Resultrecemption resultrecemption = resultrecemptionRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resultrecemption));
    }

    /**
     * DELETE  /resultrecemptions/:id : delete the "id" resultrecemption.
     *
     * @param id the id of the resultrecemption to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/resultrecemptions/{id}")
    @Timed
    public ResponseEntity<Void> deleteResultrecemption(@PathVariable Long id) {
        log.debug("REST request to delete Resultrecemption : {}", id);
        resultrecemptionRepository.delete(id);
        resultrecemptionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/resultrecemptions?query=:query : search for the resultrecemption corresponding
     * to the query.
     *
     * @param query the query of the resultrecemption search
     * @return the result of the search
     */
    @GetMapping("/_search/resultrecemptions")
    @Timed
    public List<Resultrecemption> searchResultrecemptions(@RequestParam String query) {
        log.debug("REST request to search Resultrecemptions for query {}", query);
        return StreamSupport
            .stream(resultrecemptionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}

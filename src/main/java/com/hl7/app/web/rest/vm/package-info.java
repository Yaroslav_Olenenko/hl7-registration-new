/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hl7.app.web.rest.vm;

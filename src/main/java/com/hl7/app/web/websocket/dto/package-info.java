/**
 * Data Access Objects used by WebSocket services.
 */
package com.hl7.app.web.websocket.dto;

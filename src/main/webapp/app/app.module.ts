import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { Hl7RegistrationSharedModule, UserRouteAccessService } from './shared';
import { Hl7RegistrationAppRoutingModule} from './app-routing.module';
import { Hl7RegistrationHomeModule } from './home/home.module';
import { Hl7RegistrationAdminModule } from './admin/admin.module';
import { Hl7RegistrationAccountModule } from './account/account.module';
import { Hl7RegistrationEntityModule } from './entities/entity.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        Hl7RegistrationAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        Hl7RegistrationSharedModule,
        Hl7RegistrationHomeModule,
        Hl7RegistrationAdminModule,
        Hl7RegistrationAccountModule,
        Hl7RegistrationEntityModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class Hl7RegistrationAppModule {}

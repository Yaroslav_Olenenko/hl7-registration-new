import { Component, OnInit } from '@angular/core';

import { Principal, HL7Service } from '../../shared';

@Component({
    selector: 'jhi-hl7-registration',
    templateUrl: './hl7-registration.component.html'
})
export class HL7RegistrationComponent implements OnInit {
    error: string;
    success: string;
    hl7Message: string;

    constructor(
        private hl7Service: HL7Service,
        private principal: Principal
    ) {
    }

    ngOnInit() {
        this.hl7Message =
            "MSH|^~\\&|EPIC|EPICADT|SMS|SMSADT|199912271408|CHARRIS|ADT^A04|1817457|D|2.5|\n" +
            "PID||0493575^^^2^ID 1|454721||DOE^JOHN^^^^|DOE^JOHN^^^^|19480203|M||B|254 MYSTREET AVE^^MYTOWN^OH^44123^USA||(216)123-4567|||M|NON|400003403~1129086|\n" +
            "NK1||ROE^MARIE^^^^|SPO||(216)123-4567||EC|||||||||||||||||||||||||||\n" +
            "PV1||O|168 ~219~C~PMA^^^^^^^^^||||277^ALLEN MYLASTNAME^BONNIE^^^^|||||||||| ||2688684|||||||||||||||||||||||||199912271408||||||002376853";
    }

    save() {
        this.hl7Service.create(this.hl7Message).subscribe(() => {
            this.error = null;
            this.success = 'OK';
        }, () => {
            this.success = null;
            this.error = 'ERROR';
        });
    }
}

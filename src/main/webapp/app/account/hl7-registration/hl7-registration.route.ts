import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { HL7RegistrationComponent } from './hl7-registration.component';

export const hl7RegistrationRoute: Route = {
    path: 'hl7registration',
    component: HL7RegistrationComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR'],
        pageTitle: 'HL7 Registration'
    },
    canActivate: [UserRouteAccessService]
};

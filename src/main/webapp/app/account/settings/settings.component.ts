import { Component, OnInit } from '@angular/core';

import { Principal, AccountService } from '../../shared';

@Component({
    selector: 'jhi-settings',
    templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
    error: string;
    success: string;
    settingsAccount: any;
    languages: any[];
    countres: any[];
    genders: any[];

    constructor(
        private account: AccountService,
        private principal: Principal
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.settingsAccount = this.copyAccount(account);
        });

        this.countres = [
            {id: 1, name: 'Ukraine'},
            {id: 2, name: 'USA'},
            {id: 3, name: 'Russiya'},
            {id: 4, name: 'Germany'},
            {id: 5, name: 'UK'}
        ];

        this.genders = [
            {id: 1, name: 'Female'},
            {id: 2, name: 'Male'},
            {id: 3, name: 'Not applicable'},
            {id: 4, name: 'Other'},
            {id: 5, name: 'Unknown'},
            {id: 6, name: 'Ambiguous'},
        ]
    }

    save() {
        this.account.save(this.settingsAccount).subscribe(() => {
            this.error = null;
            this.success = 'OK';
            this.principal.identity(true).then((account) => {
                this.settingsAccount = this.copyAccount(account);
            });
        }, () => {
            this.success = null;
            this.error = 'ERROR';
        });
    }

    copyAccount(account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl,
            country: account.country,
            gender: account.gender,
            birthDate: account.birthDate
        };
    }
}

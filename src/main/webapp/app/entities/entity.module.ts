import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { Hl7RegistrationRolesModule } from './roles/roles.module';
import { Hl7RegistrationCalendarrecptionModule } from './calendarrecption/calendarrecption.module';
import { Hl7RegistrationResultrecemptionModule } from './resultrecemption/resultrecemption.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        Hl7RegistrationRolesModule,
        Hl7RegistrationCalendarrecptionModule,
        Hl7RegistrationResultrecemptionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Hl7RegistrationEntityModule {}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Hl7RegistrationSharedModule } from '../../shared';
import { Hl7RegistrationAdminModule } from '../../admin/admin.module';
import {
    CalendarrecptionService,
    CalendarrecptionPopupService,
    CalendarrecptionComponent,
    CalendarrecptionDetailComponent,
    CalendarrecptionDialogComponent,
    CalendarrecptionPopupComponent,
    CalendarrecptionDeletePopupComponent,
    CalendarrecptionDeleteDialogComponent,
    calendarrecptionRoute,
    calendarrecptionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...calendarrecptionRoute,
    ...calendarrecptionPopupRoute,
];

@NgModule({
    imports: [
        Hl7RegistrationSharedModule,
        Hl7RegistrationAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CalendarrecptionComponent,
        CalendarrecptionDetailComponent,
        CalendarrecptionDialogComponent,
        CalendarrecptionDeleteDialogComponent,
        CalendarrecptionPopupComponent,
        CalendarrecptionDeletePopupComponent,
    ],
    entryComponents: [
        CalendarrecptionComponent,
        CalendarrecptionDialogComponent,
        CalendarrecptionPopupComponent,
        CalendarrecptionDeleteDialogComponent,
        CalendarrecptionDeletePopupComponent,
    ],
    providers: [
        CalendarrecptionService,
        CalendarrecptionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Hl7RegistrationCalendarrecptionModule {}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Calendarrecption } from './calendarrecption.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CalendarrecptionService {

    private resourceUrl = SERVER_API_URL + 'api/calendarrecptions';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/calendarrecptions';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(calendarrecption: Calendarrecption): Observable<Calendarrecption> {
        const copy = this.convert(calendarrecption);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(calendarrecption: Calendarrecption): Observable<Calendarrecption> {
        const copy = this.convert(calendarrecption);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Calendarrecption> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Calendarrecption.
     */
    private convertItemFromServer(json: any): Calendarrecption {
        const entity: Calendarrecption = Object.assign(new Calendarrecption(), json);
        entity.date = this.dateUtils
            .convertLocalDateFromServer(json.date);
        return entity;
    }

    /**
     * Convert a Calendarrecption to a JSON which can be sent to the server.
     */
    private convert(calendarrecption: Calendarrecption): Calendarrecption {
        const copy: Calendarrecption = Object.assign({}, calendarrecption);
        copy.date = this.dateUtils
            .convertLocalDateToServer(calendarrecption.date);
        return copy;
    }
}

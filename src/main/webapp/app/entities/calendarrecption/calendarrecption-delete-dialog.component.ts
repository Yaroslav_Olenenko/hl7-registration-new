import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Calendarrecption } from './calendarrecption.model';
import { CalendarrecptionPopupService } from './calendarrecption-popup.service';
import { CalendarrecptionService } from './calendarrecption.service';

@Component({
    selector: 'jhi-calendarrecption-delete-dialog',
    templateUrl: './calendarrecption-delete-dialog.component.html'
})
export class CalendarrecptionDeleteDialogComponent {

    calendarrecption: Calendarrecption;

    constructor(
        private calendarrecptionService: CalendarrecptionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.calendarrecptionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'calendarrecptionListModification',
                content: 'Deleted an calendarrecption'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-calendarrecption-delete-popup',
    template: ''
})
export class CalendarrecptionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private calendarrecptionPopupService: CalendarrecptionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.calendarrecptionPopupService
                .open(CalendarrecptionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

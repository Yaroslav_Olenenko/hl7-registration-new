import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Calendarrecption } from './calendarrecption.model';
import { CalendarrecptionService } from './calendarrecption.service';

@Injectable()
export class CalendarrecptionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private calendarrecptionService: CalendarrecptionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.calendarrecptionService.find(id).subscribe((calendarrecption) => {
                    if (calendarrecption.date) {
                        calendarrecption.date = {
                            year: calendarrecption.date.getFullYear(),
                            month: calendarrecption.date.getMonth() + 1,
                            day: calendarrecption.date.getDate()
                        };
                    }
                    this.ngbModalRef = this.calendarrecptionModalRef(component, calendarrecption);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.calendarrecptionModalRef(component, new Calendarrecption());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    calendarrecptionModalRef(component: Component, calendarrecption: Calendarrecption): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.calendarrecption = calendarrecption;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}

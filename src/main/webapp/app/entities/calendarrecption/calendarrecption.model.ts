import { BaseEntity, User } from './../../shared';

export class Calendarrecption implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public adress?: string,
        public calendarreceptionusers?: User[],
    ) {
    }
}

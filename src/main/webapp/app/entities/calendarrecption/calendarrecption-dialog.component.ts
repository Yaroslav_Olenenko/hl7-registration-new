import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Calendarrecption } from './calendarrecption.model';
import { CalendarrecptionPopupService } from './calendarrecption-popup.service';
import { CalendarrecptionService } from './calendarrecption.service';
import { User, UserService } from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-calendarrecption-dialog',
    templateUrl: './calendarrecption-dialog.component.html'
})
export class CalendarrecptionDialogComponent implements OnInit {

    calendarrecption: Calendarrecption;
    isSaving: boolean;

    users: User[];
    dateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private calendarrecptionService: CalendarrecptionService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.calendarrecption.id !== undefined) {
            this.subscribeToSaveResponse(
                this.calendarrecptionService.update(this.calendarrecption));
        } else {
            this.subscribeToSaveResponse(
                this.calendarrecptionService.create(this.calendarrecption));
        }
    }

    private subscribeToSaveResponse(result: Observable<Calendarrecption>) {
        result.subscribe((res: Calendarrecption) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Calendarrecption) {
        this.eventManager.broadcast({ name: 'calendarrecptionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-calendarrecption-popup',
    template: ''
})
export class CalendarrecptionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private calendarrecptionPopupService: CalendarrecptionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.calendarrecptionPopupService
                    .open(CalendarrecptionDialogComponent as Component, params['id']);
            } else {
                this.calendarrecptionPopupService
                    .open(CalendarrecptionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

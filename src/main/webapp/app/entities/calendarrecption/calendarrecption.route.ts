import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CalendarrecptionComponent } from './calendarrecption.component';
import { CalendarrecptionDetailComponent } from './calendarrecption-detail.component';
import { CalendarrecptionPopupComponent } from './calendarrecption-dialog.component';
import { CalendarrecptionDeletePopupComponent } from './calendarrecption-delete-dialog.component';

export const calendarrecptionRoute: Routes = [
    {
        path: 'calendarrecption',
        component: CalendarrecptionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Calendarrecptions'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'calendarrecption/:id',
        component: CalendarrecptionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Calendarrecptions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const calendarrecptionPopupRoute: Routes = [
    {
        path: 'calendarrecption-new',
        component: CalendarrecptionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Calendarrecptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'calendarrecption/:id/edit',
        component: CalendarrecptionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Calendarrecptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'calendarrecption/:id/delete',
        component: CalendarrecptionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Calendarrecptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

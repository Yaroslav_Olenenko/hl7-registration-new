import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Calendarrecption } from './calendarrecption.model';
import { CalendarrecptionService } from './calendarrecption.service';

@Component({
    selector: 'jhi-calendarrecption-detail',
    templateUrl: './calendarrecption-detail.component.html'
})
export class CalendarrecptionDetailComponent implements OnInit, OnDestroy {

    calendarrecption: Calendarrecption;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private calendarrecptionService: CalendarrecptionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCalendarrecptions();
    }

    load(id) {
        this.calendarrecptionService.find(id).subscribe((calendarrecption) => {
            this.calendarrecption = calendarrecption;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCalendarrecptions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'calendarrecptionListModification',
            (response) => this.load(this.calendarrecption.id)
        );
    }
}

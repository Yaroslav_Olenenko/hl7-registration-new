import { BaseEntity, User } from './../../shared';

export class Roles implements BaseEntity {
    constructor(
        public id?: number,
        public roleName?: string,
        public userToRoles?: User[],
    ) {
    }
}

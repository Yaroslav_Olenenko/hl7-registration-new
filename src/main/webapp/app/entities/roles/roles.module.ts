import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Hl7RegistrationSharedModule } from '../../shared';
import { Hl7RegistrationAdminModule } from '../../admin/admin.module';
import {
    RolesService,
    RolesPopupService,
    RolesComponent,
    RolesDetailComponent,
    RolesDialogComponent,
    RolesPopupComponent,
    RolesDeletePopupComponent,
    RolesDeleteDialogComponent,
    rolesRoute,
    rolesPopupRoute,
} from './';

const ENTITY_STATES = [
    ...rolesRoute,
    ...rolesPopupRoute,
];

@NgModule({
    imports: [
        Hl7RegistrationSharedModule,
        Hl7RegistrationAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RolesComponent,
        RolesDetailComponent,
        RolesDialogComponent,
        RolesDeleteDialogComponent,
        RolesPopupComponent,
        RolesDeletePopupComponent,
    ],
    entryComponents: [
        RolesComponent,
        RolesDialogComponent,
        RolesPopupComponent,
        RolesDeleteDialogComponent,
        RolesDeletePopupComponent,
    ],
    providers: [
        RolesService,
        RolesPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Hl7RegistrationRolesModule {}

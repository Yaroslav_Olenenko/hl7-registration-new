import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Resultrecemption } from './resultrecemption.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ResultrecemptionService {

    private resourceUrl = SERVER_API_URL + 'api/resultrecemptions';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/resultrecemptions';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(resultrecemption: Resultrecemption): Observable<Resultrecemption> {
        const copy = this.convert(resultrecemption);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(resultrecemption: Resultrecemption): Observable<Resultrecemption> {
        const copy = this.convert(resultrecemption);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Resultrecemption> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Resultrecemption.
     */
    private convertItemFromServer(json: any): Resultrecemption {
        const entity: Resultrecemption = Object.assign(new Resultrecemption(), json);
        entity.date = this.dateUtils
            .convertDateTimeFromServer(json.date);
        return entity;
    }

    /**
     * Convert a Resultrecemption to a JSON which can be sent to the server.
     */
    private convert(resultrecemption: Resultrecemption): Resultrecemption {
        const copy: Resultrecemption = Object.assign({}, resultrecemption);

        copy.date = this.dateUtils.toDate(resultrecemption.date);
        return copy;
    }
}

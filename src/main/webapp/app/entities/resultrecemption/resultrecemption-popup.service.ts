import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Resultrecemption } from './resultrecemption.model';
import { ResultrecemptionService } from './resultrecemption.service';

@Injectable()
export class ResultrecemptionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private resultrecemptionService: ResultrecemptionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.resultrecemptionService.find(id).subscribe((resultrecemption) => {
                    resultrecemption.date = this.datePipe
                        .transform(resultrecemption.date, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.resultrecemptionModalRef(component, resultrecemption);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.resultrecemptionModalRef(component, new Resultrecemption());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    resultrecemptionModalRef(component: Component, resultrecemption: Resultrecemption): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.resultrecemption = resultrecemption;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}

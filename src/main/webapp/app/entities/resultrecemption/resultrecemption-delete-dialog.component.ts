import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Resultrecemption } from './resultrecemption.model';
import { ResultrecemptionPopupService } from './resultrecemption-popup.service';
import { ResultrecemptionService } from './resultrecemption.service';

@Component({
    selector: 'jhi-resultrecemption-delete-dialog',
    templateUrl: './resultrecemption-delete-dialog.component.html'
})
export class ResultrecemptionDeleteDialogComponent {

    resultrecemption: Resultrecemption;

    constructor(
        private resultrecemptionService: ResultrecemptionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.resultrecemptionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'resultrecemptionListModification',
                content: 'Deleted an resultrecemption'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-resultrecemption-delete-popup',
    template: ''
})
export class ResultrecemptionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private resultrecemptionPopupService: ResultrecemptionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.resultrecemptionPopupService
                .open(ResultrecemptionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Hl7RegistrationSharedModule } from '../../shared';
import { Hl7RegistrationAdminModule } from '../../admin/admin.module';
import {
    ResultrecemptionService,
    ResultrecemptionPopupService,
    ResultrecemptionComponent,
    ResultrecemptionDetailComponent,
    ResultrecemptionDialogComponent,
    ResultrecemptionPopupComponent,
    ResultrecemptionDeletePopupComponent,
    ResultrecemptionDeleteDialogComponent,
    resultrecemptionRoute,
    resultrecemptionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...resultrecemptionRoute,
    ...resultrecemptionPopupRoute,
];

@NgModule({
    imports: [
        Hl7RegistrationSharedModule,
        Hl7RegistrationAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ResultrecemptionComponent,
        ResultrecemptionDetailComponent,
        ResultrecemptionDialogComponent,
        ResultrecemptionDeleteDialogComponent,
        ResultrecemptionPopupComponent,
        ResultrecemptionDeletePopupComponent,
    ],
    entryComponents: [
        ResultrecemptionComponent,
        ResultrecemptionDialogComponent,
        ResultrecemptionPopupComponent,
        ResultrecemptionDeleteDialogComponent,
        ResultrecemptionDeletePopupComponent,
    ],
    providers: [
        ResultrecemptionService,
        ResultrecemptionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Hl7RegistrationResultrecemptionModule {}

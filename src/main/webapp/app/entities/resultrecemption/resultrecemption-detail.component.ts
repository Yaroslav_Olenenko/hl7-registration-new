import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Resultrecemption } from './resultrecemption.model';
import { ResultrecemptionService } from './resultrecemption.service';

@Component({
    selector: 'jhi-resultrecemption-detail',
    templateUrl: './resultrecemption-detail.component.html'
})
export class ResultrecemptionDetailComponent implements OnInit, OnDestroy {

    resultrecemption: Resultrecemption;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private resultrecemptionService: ResultrecemptionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInResultrecemptions();
    }

    load(id) {
        this.resultrecemptionService.find(id).subscribe((resultrecemption) => {
            this.resultrecemption = resultrecemption;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInResultrecemptions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'resultrecemptionListModification',
            (response) => this.load(this.resultrecemption.id)
        );
    }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Resultrecemption } from './resultrecemption.model';
import { ResultrecemptionPopupService } from './resultrecemption-popup.service';
import { ResultrecemptionService } from './resultrecemption.service';
import { Calendarrecption, CalendarrecptionService } from '../calendarrecption';
import { User, UserService } from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-resultrecemption-dialog',
    templateUrl: './resultrecemption-dialog.component.html'
})
export class ResultrecemptionDialogComponent implements OnInit {

    resultrecemption: Resultrecemption;
    isSaving: boolean;

    resultcalendarreceptions: Calendarrecption[];

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private resultrecemptionService: ResultrecemptionService,
        private calendarrecptionService: CalendarrecptionService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.calendarrecptionService
            .query({filter: 'resultrecemption-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.resultrecemption.resultcalendarreception || !this.resultrecemption.resultcalendarreception.id) {
                    this.resultcalendarreceptions = res.json;
                } else {
                    this.calendarrecptionService
                        .find(this.resultrecemption.resultcalendarreception.id)
                        .subscribe((subRes: Calendarrecption) => {
                            this.resultcalendarreceptions = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.resultrecemption.id !== undefined) {
            this.subscribeToSaveResponse(
                this.resultrecemptionService.update(this.resultrecemption));
        } else {
            this.subscribeToSaveResponse(
                this.resultrecemptionService.create(this.resultrecemption));
        }
    }

    private subscribeToSaveResponse(result: Observable<Resultrecemption>) {
        result.subscribe((res: Resultrecemption) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Resultrecemption) {
        this.eventManager.broadcast({ name: 'resultrecemptionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCalendarrecptionById(index: number, item: Calendarrecption) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-resultrecemption-popup',
    template: ''
})
export class ResultrecemptionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private resultrecemptionPopupService: ResultrecemptionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.resultrecemptionPopupService
                    .open(ResultrecemptionDialogComponent as Component, params['id']);
            } else {
                this.resultrecemptionPopupService
                    .open(ResultrecemptionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

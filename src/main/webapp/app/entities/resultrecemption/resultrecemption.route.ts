import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ResultrecemptionComponent } from './resultrecemption.component';
import { ResultrecemptionDetailComponent } from './resultrecemption-detail.component';
import { ResultrecemptionPopupComponent } from './resultrecemption-dialog.component';
import { ResultrecemptionDeletePopupComponent } from './resultrecemption-delete-dialog.component';

export const resultrecemptionRoute: Routes = [
    {
        path: 'resultrecemption',
        component: ResultrecemptionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Resultrecemptions'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'resultrecemption/:id',
        component: ResultrecemptionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Resultrecemptions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const resultrecemptionPopupRoute: Routes = [
    {
        path: 'resultrecemption-new',
        component: ResultrecemptionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Resultrecemptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'resultrecemption/:id/edit',
        component: ResultrecemptionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Resultrecemptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'resultrecemption/:id/delete',
        component: ResultrecemptionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Resultrecemptions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

import { BaseEntity, User } from './../../shared';

export class Resultrecemption implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public diagnosis?: string,
        public procedures?: string,
        public status?: number,
        public treatmentplan?: string,
        public resultcalendarreception?: BaseEntity,
        public users?: User[],
    ) {
    }
}

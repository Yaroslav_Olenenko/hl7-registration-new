import { platformBrowser } from '@angular/platform-browser';
import { ProdConfig } from './blocks/config/prod.config';
import { Hl7RegistrationAppModuleNgFactory } from '../../../../target/aot/src/main/webapp/app/app.module.ngfactory';

ProdConfig();

platformBrowser().bootstrapModuleFactory(Hl7RegistrationAppModuleNgFactory)
.then((success) => console.log(`Application started`))
.catch((err) => console.error(err));
